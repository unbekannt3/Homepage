import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { URLShorteningErrorComponent } from './urlshortening-error.component';

describe('URLShorteningErrorComponent', () => {
  let component: URLShorteningErrorComponent;
  let fixture: ComponentFixture<URLShorteningErrorComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ URLShorteningErrorComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(URLShorteningErrorComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

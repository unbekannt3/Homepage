import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-git-how-to',
  templateUrl: './git-how-to.component.html',
  styleUrls: ['./git-how-to.component.scss']
})
export class GitHowToComponent implements OnInit {

  private gitlabStatusCode = 0;

  constructor(private route: ActivatedRoute) {
    this.route.params.subscribe(params => {
      this.gitlabStatusCode = +params['sc']; // (+) converts string 'sc' to a number
    });
    console.log('GitLab redirect statuscode: ' + this.gitlabStatusCode);
  }

  @ViewChild('alert') alert: ElementRef;

  closeAlert() {
    this.alert.nativeElement.classList.remove('show');
  }

  ngOnInit() {
  }

  get gsc() {
    return this.gitlabStatusCode;
  }

}

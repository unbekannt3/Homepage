import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GitHowToComponent } from './git-how-to.component';

describe('GitHowToComponent', () => {
  let component: GitHowToComponent;
  let fixture: ComponentFixture<GitHowToComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GitHowToComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GitHowToComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

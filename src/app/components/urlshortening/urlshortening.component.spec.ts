import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { URLShorteningComponent } from './urlshortening.component';

describe('URLShorteningComponent', () => {
  let component: URLShorteningComponent;
  let fixture: ComponentFixture<URLShorteningComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ URLShorteningComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(URLShorteningComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

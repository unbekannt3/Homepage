import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
// import { Angulartics2UirouterModule } from 'angulartics2/uiroutermodule';
import { Angulartics2RouterlessModule } from 'angulartics2/routerlessmodule';
import { ScrollToModule } from '@nicky-lenaers/ngx-scroll-to';
import { NgxGistModule } from 'ngx-gist/dist/ngx-gist.module';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { MDBBootstrapModule } from 'angular-bootstrap-md';
import { RouterModule } from '@angular/router';
import { NavbarComponent } from './components/navbar/navbar.component';
import { GitHowToComponent } from './components/git-how-to/git-how-to.component';
import { URLShorteningComponent } from './components/urlshortening/urlshortening.component';
import { URLShorteningErrorComponent } from './components/urlshortening-error/urlshortening-error.component';
import { FooterComponent } from './components/footer/footer.component';
import { HomeComponent } from './components/home/home.component';
import { ProjectsComponent } from './components/projects/projects.component';
import { PrivacyPolicyComponent } from './components/gov/privacy-policy/privacy-policy.component';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { AboutMeComponent } from './components/about-me/about-me.component';
import { APP_BASE_HREF } from '@angular/common';
import { BootstrapBasicsComponent } from './components/tutorials/bootstrap-basics/bootstrap-basics.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    GitHowToComponent,
    URLShorteningComponent,
    URLShorteningErrorComponent,
    FooterComponent,
    HomeComponent,
    ProjectsComponent,
    PrivacyPolicyComponent,
    NotFoundComponent,
    AboutMeComponent,
    BootstrapBasicsComponent
  ],
  imports: [
    BrowserModule.withServerTransition({ appId: 'serverApp' }),
    AppRoutingModule,
    MDBBootstrapModule.forRoot(),
    RouterModule,
    // Angulartics2UirouterModule.forRoot(),
    Angulartics2RouterlessModule.forRoot(),
    ScrollToModule.forRoot(),
    NgxGistModule
  ],
  providers: [{ provide: APP_BASE_HREF, useValue: '/' }],
  bootstrap: [AppComponent]
})
export class AppModule { }

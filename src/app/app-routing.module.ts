import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { NotFoundComponent } from './components/not-found/not-found.component';
import { PrivacyPolicyComponent } from './components/gov/privacy-policy/privacy-policy.component';
import { URLShorteningErrorComponent } from './components/urlshortening-error/urlshortening-error.component';
import { URLShorteningComponent } from './components/urlshortening/urlshortening.component';
import { GitHowToComponent } from './components/git-how-to/git-how-to.component';
import { ProjectsComponent } from './components/projects/projects.component';
import { AboutMeComponent } from './components/about-me/about-me.component';
import { HomeComponent } from './components/home/home.component';
import { BootstrapBasicsComponent } from './components/tutorials/bootstrap-basics/bootstrap-basics.component';

const routes: Routes = [
  // Index
  {
    path: 'home',
    component: HomeComponent
  },
  // About Me
  {
    path: 'me',
    component: AboutMeComponent
  },
  // Projects
  {
    path: 'projects',
    component: ProjectsComponent
  },
  // Git How-To
  {
    path: 'git',
    component: GitHowToComponent
  },
  {
    path: 'git/glrsc/:sc',
    component: GitHowToComponent
  },
  {
    path: 'tutorials/bootstrap/basics',
    component: BootstrapBasicsComponent
  },
  // URL Shortening Info
  {
    path: 'urlshortening',
    component: URLShorteningComponent
  },
  // URL Shortcut not found
  {
    path: 'urlserror',
    component: URLShorteningErrorComponent
  },
  // Dataprivacy
  {
    path: 'gov/privacy',
    component: PrivacyPolicyComponent
  },
  // /imprint > /gov/privacy
  {
    path: 'imprint',
    redirectTo: 'gov/privacy',
    pathMatch: 'full'
  },

  // / > home
  {
    path: '',
    redirectTo: 'home',
    pathMatch: 'full'
  },
  /*****************/
  { path: '', redirectTo: '', pathMatch: 'full' },
  // nicht vorhandene Routes zu NotFoundComponent
  {
    path: '**',
    component: NotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { scrollPositionRestoration: 'enabled' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }

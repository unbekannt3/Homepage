import { Component, OnDestroy, OnInit } from '@angular/core';
import { Angulartics2Piwik } from 'angulartics2/piwik';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'unbekannt3.eu';

  constructor(private angulartics2Piwik: Angulartics2Piwik) {
    angulartics2Piwik.startTracking();
  }
}
